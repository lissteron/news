module gitlab.com/lissteron/news

go 1.13

require (
	github.com/fasthttp/router v0.6.1
	github.com/golang/protobuf v1.3.2
	github.com/jmoiron/sqlx v1.2.0
	github.com/lib/pq v1.3.0
	github.com/mailru/easyjson v0.7.1
	github.com/nats-io/nats-streaming-server v0.17.0 // indirect
	github.com/nats-io/stan.go v0.6.0
	github.com/spf13/viper v1.6.2
	github.com/valyala/fasthttp v1.9.0
	go.uber.org/automaxprocs v1.3.0
	go.uber.org/zap v1.14.0
)
