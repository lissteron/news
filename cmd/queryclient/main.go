package main

import (
	"context"
	"log"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"gitlab.com/lissteron/news/configs"
	"gitlab.com/lissteron/news/internal/client"
	"gitlab.com/lissteron/news/internal/pkg/stan"
	"gitlab.com/lissteron/news/pkg/sigwaiter"

	_ "github.com/lib/pq"
	_ "go.uber.org/automaxprocs"
)

func main() {
	// Читаем конфиг
	configs.Read()

	ctx, cancel := context.WithCancel(context.Background())

	cfg := zap.NewDevelopmentConfig()

	// Создаем логер
	logger, err := cfg.Build()
	if err != nil {
		log.Fatalln(err)
	}

	sugar := logger.Sugar()

	stan := stan.NewStan(sugar,
		viper.GetString("STAN_URI"),
		viper.GetString("STAN_CLIENT_ID"),
		viper.GetString("STAN_CLUSTER_NAME"))

	if err = stan.Connect(); err != nil {
		log.Fatalf("can't connect to stan: %v", err)
	}

	swaiter := sigwaiter.Build(viper.GetDuration("EXIT_TIMEOUT"), nil)

	c := client.NewClient(sugar, swaiter, stan, viper.GetUint("HTTP_PORT"), viper.GetUint("HTTP_MAX_BODY_SIZE"))
	c.Router.GET(`/news/get/:id`, c.NewsGet)

	if err = c.Subscribe(); err != nil {
		log.Fatalf("can't subsctibe to stan: %v", err)
	}

	go func() {
		err = c.RunServer(ctx)
		if err != nil {
			sugar.Errorf("query client error: %v", err)
		}
	}()

	sugar.Info("queryclient inited")

	swaiter.WaitAll(cancel)
}
