package main

import (
	"context"
	"fmt"
	"log"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"gitlab.com/lissteron/news/configs"
	"gitlab.com/lissteron/news/internal/pkg/stan"
	"gitlab.com/lissteron/news/internal/storage"
	"gitlab.com/lissteron/news/pkg/sigwaiter"

	_ "github.com/lib/pq"
	_ "go.uber.org/automaxprocs"
)

func main() {
	// Читаем конфиг
	configs.Read()

	ctx, cancel := context.WithCancel(context.Background())

	cfg := zap.NewDevelopmentConfig()

	// Создаем логер
	logger, err := cfg.Build()
	if err != nil {
		log.Fatalln(err)
	}

	sugar := logger.Sugar()

	stan := stan.NewStan(sugar,
		viper.GetString("STAN_URI"),
		viper.GetString("STAN_CLIENT_ID"),
		viper.GetString("STAN_CLUSTER_NAME"))

	if err = stan.Connect(); err != nil {
		log.Fatalf("can't connect to stan: %v", err)
	}

	swaiter := sigwaiter.Build(viper.GetDuration("EXIT_TIMEOUT"), nil)

	repo := storage.NewCockroachDB(sugar)
	if err = repo.Connect(ctx, fmt.Sprintf("postgresql://%s@%s/%s?sslmode=disable",
		viper.GetString("DATABASE_LOGIN"),
		viper.GetString("DATABASE_HOST"),
		viper.GetString("DATABASE_DATABASE"),
	)); err != nil {
		log.Fatalf("can't connect to db: %v", err)
	}

	stor := storage.NewStorage(ctx, sugar, swaiter, repo, stan, viper.GetDuration("STAN_TTL"))
	if err = stor.Run(); err != nil {
		log.Fatalln(err)
	}

	sugar.Info("storage inited")

	swaiter.WaitAll(cancel)
}
