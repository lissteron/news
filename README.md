# news

## Требования
1. docker
2. docker-compose
3. golang

## Запуск
```
sudo docker-compose up
```

## Инициализация базы
```
make initdb
```

## Тестирование
```
make gotest
```