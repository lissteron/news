package sigwaiter

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"

	"go.uber.org/zap"
)

// Waiter - Объект ждуна
type Waiter struct {
	exitChan    chan struct{}
	WaitTimeout time.Duration
	Logger      *zap.SugaredLogger
	wg          sync.WaitGroup
}

// Build - Создаем обработчик выхода
func Build(timeout time.Duration, logger *zap.SugaredLogger) (w *Waiter) {
	return &Waiter{
		exitChan:    make(chan struct{}),
		WaitTimeout: timeout,
		Logger:      logger,
	}
}

// WaitAll - Функция ожидания сигналов выхода
func (w *Waiter) WaitAll(cancel func()) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGTERM, os.Interrupt)

	select {
	case s := <-c:
		if w.Logger != nil {
			w.Logger.Infof("Received signal: %v", s)
		}

		close(w.exitChan)
	case <-w.exitChan:
		if w.Logger != nil {
			w.Logger.Info("self exit")
		}
	}

	go func() {
		time.Sleep(w.WaitTimeout)

		if w.Logger != nil {
			w.Logger.Errorf("Force exit after: %v", w.WaitTimeout)
		}

		cancel()
	}()

	w.wg.Wait()
}

// Add - Добавляем процесс ожидания
func (w *Waiter) Add() *Waiter {
	w.wg.Add(1)
	return w
}

// Done - Завершаем процесс ожидания
func (w *Waiter) Done() {
	w.wg.Done()
}

// Wait - Ожидаем сигнала на выход
func (w *Waiter) Wait() {
	<-w.exitChan
}

// WaitChan - Получаем сигнал ожидания выхода
func (w *Waiter) WaitChan() <-chan struct{} {
	return w.exitChan
}

// NeedExit - Определяем надо ли выходить
func (w *Waiter) NeedExit() bool {
	select {
	case <-w.exitChan:
		return true
	default:
		return false
	}
}

// Exit - Выходим
func (w *Waiter) Exit() {
	close(w.exitChan)
}
