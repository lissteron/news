package stan

import (
	"errors"
	"strconv"

	"github.com/nats-io/stan.go"
	"go.uber.org/zap"
)

type Stan struct {
	conn     stan.Conn
	connOpts []stan.Option

	uri         string
	clientID    string
	clusterName string

	logger *zap.SugaredLogger
}

func NewStan(logger *zap.SugaredLogger, uri, clientID, clusterName string) *Stan {
	return &Stan{
		uri:         uri,
		clusterName: clusterName,
		clientID:    clientID,
		logger:      logger,
	}
}

func (s *Stan) Connect(options ...stan.Option) (err error) {
	s.connOpts = options

	options = append(options, stan.NatsURL(s.uri))

	var (
		i        int
		clientID = s.clientID
	)

	// Ищем свободный clientID
	for {
		s.conn, err = stan.Connect(s.clusterName, clientID, options...)
		if err != nil {
			if err.Error() == "stan: clientID already registered" {
				i++
				clientID = s.clientID + strconv.Itoa(i)

				continue
			}

			return err
		}

		break
	}

	return nil
}

func (s *Stan) Publish(subj string, data []byte) (err error) {
	if s.conn == nil {
		return errors.New("connection is nil")
	}

	return s.conn.Publish(subj, data)
}

func (s *Stan) Subscribe(
	subj string,
	handler func(msg *stan.Msg),
	opts ...stan.SubscriptionOption,
) (stan.Subscription, error) {
	return s.conn.Subscribe(subj, handler, opts...)
}

func (s *Stan) QueueSubscribe(
	subj, qgroup string,
	handler func(msg *stan.Msg),
	opts ...stan.SubscriptionOption,
) (stan.Subscription, error) {
	return s.conn.QueueSubscribe(subj, qgroup, handler, opts...)
}
