package client

import (
	"net/http"
	"strconv"

	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"gitlab.com/lissteron/news/internal/domain"
)

func (c *Client) NewsGet(ctx *fasthttp.RequestCtx) {
	if !c.checkServiceAvailable(ctx) {
		return
	}

	c.wg.Add(1)
	defer c.wg.Done()

	var news domain.News
	if id, ok := ctx.UserValue("id").(string); ok {
		news.ID, _ = strconv.ParseInt(id, 10, 64)
	}

	if news.ID <= 0 {
		ctx.Error("Not Found", http.StatusNotFound)
		return
	}

	answer, err := c.sendNewsToStan(viper.GetString("STAN_STORAGE_GET_SUBJ"), &news)
	if err != nil {
		ctx.Error(err.Error(), http.StatusInternalServerError)
		return
	}

	if answer.ID == 0 {
		ctx.Error("Not Found", http.StatusNotFound)
		return
	}

	b, err := answer.MarshalJSON()
	if err != nil {
		c.Logger.Errorf("news marshal error: %v", err)
		ctx.Error(err.Error(), http.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json; charset=utf-8")
	ctx.SetStatusCode(200)
	ctx.Response.SetBody(b)
}
