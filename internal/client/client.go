package client

import (
	"bytes"
	"math/rand"
	"net/http"
	"strconv"
	"sync"
	"time"

	"github.com/fasthttp/router"
	"github.com/valyala/fasthttp"
	"go.uber.org/zap"
	"gitlab.com/lissteron/news/internal/domain"
	"gitlab.com/lissteron/news/internal/pkg/stan"
	"gitlab.com/lissteron/news/pkg/sigwaiter"
)

type Client struct {
	Logger     *zap.SugaredLogger
	HTTPServer fasthttp.Server
	Port       uint
	Router     *router.Router
	Cache      Cache
	Stan       *stan.Stan
	SWaiter    *sigwaiter.Waiter
	closeChan  chan struct{}
	wg         sync.WaitGroup
	rand       rand.Source
}

func NewClient(
	logger *zap.SugaredLogger,
	swaiter *sigwaiter.Waiter,
	stan *stan.Stan,
	port, maxRequestBodySize uint,
) *Client {
	r := router.New()

	qc := &Client{
		HTTPServer: fasthttp.Server{
			Handler:            r.Handler,
			MaxRequestBodySize: int(maxRequestBodySize),
		},
		Logger:    logger,
		Port:      port,
		Router:    r,
		SWaiter:   swaiter,
		Cache:     Cache{Data: make(map[string]chan *domain.News)},
		Stan:      stan,
		closeChan: make(chan struct{}),
		rand:      rand.NewSource(time.Now().UnixNano()),
	}

	return qc
}

func (c *Client) checkServiceAvailable(ctx *fasthttp.RequestCtx) bool {
	if c.serverClosed() {
		ctx.Error("Service Unavailable", http.StatusServiceUnavailable)
		return false
	}

	return true
}

func (c *Client) newsParse(ctx *fasthttp.RequestCtx) (news domain.News, err error) {
	if err = news.UnmarshalJSON(ctx.Request.Body()); err != nil {
		ctx.Error("Internal Server Error", http.StatusInternalServerError)
		return
	}

	return
}

func (c *Client) sendNewsToStan(subj string, news *domain.News) (answer *domain.News, err error) {
	// Создаем ключ идемпотентности
	news.RequestKey = c.createRequestKey()

	// Создаем канал для получения ответа по ствну
	retryChan := make(chan *domain.News, 1)
	defer close(retryChan)

	// Сохраняем канал для ответа
	c.Cache.Set(news.RequestKey, retryChan)

	proto := domain.ConvertToNewsProto(news)

	req, err := proto.Marshal()
	if err != nil {
		c.Logger.Errorf("can't marshal proto: %v", err)
		return
	}

	// Отправляем в стан запрос
	if err = c.Stan.Publish(subj, req); err != nil {
		c.Logger.Errorf("can't send news to stan: %v", err)
		return
	}

	// Ждем ответ
	answer = <-retryChan

	return answer, nil
}

func (c *Client) createRequestKey() string {
	buf := bytes.NewBufferString(strconv.FormatInt(time.Now().UnixNano(), 32))
	buf.WriteString(strconv.FormatInt(c.rand.Int63(), 32))

	return buf.String()
}
