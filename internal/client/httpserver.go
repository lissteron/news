package client

import (
	"context"
	"fmt"
	"net"
)

func (c *Client) RunServer(ctx context.Context) (err error) {
	defer c.SWaiter.Add().Done()

	listener, err := c.newTCPListener()
	if err != nil {
		c.Logger.Errorf("listener error: %v", err)
		return
	}

	if err = c.HTTPServer.Serve(listener); err != nil {
		c.Logger.Errorf("serve error: %v", err)
		return
	}

	return
}

func (c *Client) serverClosed() bool {
	select {
	case <-c.closeChan:
		return true
	default:
		return false
	}
}

func (c *Client) newTCPListener() (net.Listener, error) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", c.Port))
	if err != nil {
		return nil, err
	}

	go func() {
		select {
		case <-c.closeChan:
		case <-c.SWaiter.WaitChan():
			close(c.closeChan)
		}

		c.wg.Wait()

		if err := listener.Close(); err != nil {
			c.Logger.Errorf("listener error: %v", err)
		}
	}()

	return listener, nil
}
