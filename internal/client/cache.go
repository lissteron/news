package client

import (
	"sync"

	"gitlab.com/lissteron/news/internal/domain"
)

type Cache struct {
	sync.RWMutex
	Data map[string]chan *domain.News
}

func (c *Cache) Get(key string) (chan *domain.News, bool) {
	c.RLock()
	defer c.RUnlock()

	ch, exist := c.Data[key]
	if exist {
		delete(c.Data, key)
	}

	return ch, exist
}

func (c *Cache) Set(key string, ch chan *domain.News) {
	c.Lock()
	defer c.Unlock()

	c.Data[key] = ch
}
