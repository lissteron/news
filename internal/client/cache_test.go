package client

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/lissteron/news/internal/domain"
)

func TestCache_Get(t *testing.T) {
	type fields struct {
		RWMutex sync.RWMutex
		Data    map[string]chan *domain.News
	}
	type args struct {
		key string
	}

	ch1 := make(chan *domain.News)

	tests := []struct {
		name   string
		fields fields
		args   args
		want   chan *domain.News
		want1  bool
	}{
		{
			name: "pass",
			fields: fields{
				Data: map[string]chan *domain.News{"t1": ch1, "t2": make(chan *domain.News)},
			},
			args:  args{key: "t1"},
			want:  ch1,
			want1: true,
		},
		{
			name: "not exist",
			fields: fields{
				Data: make(map[string]chan *domain.News),
			},
			args:  args{key: "t1"},
			want:  nil,
			want1: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Cache{
				RWMutex: tt.fields.RWMutex,
				Data:    tt.fields.Data,
			}
			got, got1 := c.Get(tt.args.key)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Cache.Get() got = %v, want %v", got, tt.want)
			}
			if got1 != tt.want1 {
				t.Errorf("Cache.Get() got1 = %v, want %v", got1, tt.want1)
			}
		})
	}
}
