package client

import (
	"github.com/nats-io/stan.go"
	"github.com/spf13/viper"
	v1 "gitlab.com/lissteron/news/api/v1"
	"gitlab.com/lissteron/news/internal/domain"
)

func (c *Client) Subscribe() (err error) {
	_, err = c.Stan.Subscribe(viper.GetString("STAN_CLIENT_SUBJ"), c.ParseStanMsg)
	if err != nil {
		c.Logger.Error("can't subscribe stan: %v", err)
		return
	}

	return
}

func (c *Client) ParseStanMsg(msg *stan.Msg) {
	var proto v1.News
	if err := proto.Unmarshal(msg.Data); err != nil {
		c.Logger.Errorf("can't unmarshal proto: %v", err)
		return
	}

	news := domain.ConvertToNewsDomain(&proto)

	if ch, exist := c.Cache.Get(news.RequestKey); exist {
		ch <- news
	}
}
