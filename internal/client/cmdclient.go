package client

import (
	"errors"
	"net/http"
	"time"

	"github.com/spf13/viper"
	"github.com/valyala/fasthttp"
	"gitlab.com/lissteron/news/internal/domain"
)

// NewsCreate - создаем новость
func (c *Client) NewsCreate(ctx *fasthttp.RequestCtx) {
	if !c.checkServiceAvailable(ctx) {
		return
	}

	c.wg.Add(1)
	defer c.wg.Done()

	news, err := c.newsParse(ctx)
	if err != nil {
		c.Logger.Errorf("news parse error: %v", err)
		return
	}

	// Валидируем новость.
	if err := validateNews(&news); err != nil {
		ctx.Error(err.Error(), http.StatusBadRequest)
		return
	}

	// Создаем ключ идемпотентности
	if err = news.СreateIdempotenceKey(); err != nil {
		c.Logger.Errorf("create idempotence key error: %v", err)
		ctx.Error("Internal Server Error", http.StatusInternalServerError)

		return
	}

	answer, err := c.sendNewsToStan(viper.GetString("STAN_STORAGE_CREATE_SUBJ"), &news)
	if err != nil {
		ctx.Error(err.Error(), http.StatusInternalServerError)
		return
	}

	b, err := answer.MarshalJSON()
	if err != nil {
		c.Logger.Errorf("news marshal error: %v", err)
		ctx.Error(err.Error(), http.StatusInternalServerError)

		return
	}

	ctx.SetContentType("application/json; charset=utf-8")
	ctx.SetStatusCode(201)
	ctx.Response.SetBody(b)
}

func validateNews(news *domain.News) (err error) {
	if news.Title == "" {
		return errors.New("news title empty")
	}

	if time.Now().Before(news.Date) {
		return errors.New("news date from the future")
	}

	return
}
