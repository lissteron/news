package domain

import (
	"reflect"
	"testing"
	"time"

	v1 "gitlab.com/lissteron/news/api/v1"
)

func TestConvertToNewsDomain(t *testing.T) {
	type args struct {
		news *v1.News
	}
	tests := []struct {
		name string
		args args
		want *News
	}{
		{
			name: "pass",
			args: args{news: &v1.News{
				ID:             1,
				IdempotenceKey: []byte("test"),
				Title:          "title",
				Date:           time.Unix(12345678, 0).UnixNano(),
				RequestKey:     "r1",
			}},
			want: &News{
				ID:             1,
				IdempotenceKey: []byte("test"),
				Title:          "title",
				Date:           time.Unix(12345678, 0),
				RequestKey:     "r1",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConvertToNewsDomain(tt.args.news); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ConvertToNewsDomain() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestConvertToNewsProto(t *testing.T) {
	type args struct {
		news *News
	}
	tests := []struct {
		name string
		args args
		want *v1.News
	}{
		{
			name: "pass",
			args: args{news: &News{
				ID:             1,
				IdempotenceKey: []byte("test"),
				Title:          "title",
				Date:           time.Unix(12345678, 0),
				RequestKey:     "r1",
			}},
			want: &v1.News{
				ID:             1,
				IdempotenceKey: []byte("test"),
				Title:          "title",
				Date:           time.Unix(12345678, 0).UnixNano(),
				RequestKey:     "r1",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConvertToNewsProto(tt.args.news); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ConvertToNewsProto() = %v, want %v", got, tt.want)
			}
		})
	}
}
