package domain

import (
	"crypto/sha256"
	"time"

	v1 "gitlab.com/lissteron/news/api/v1"
)

//easyjson:json
type News struct {
	RequestKey     string    `json:"-"`
	IdempotenceKey []byte    `json:"-"`
	ID             int64     `json:"id"`
	Title          string    `json:"title"`
	Date           time.Time `json:"date"`
}

func (n *News) СreateIdempotenceKey() (err error) {
	hash := sha256.New()

	if _, err = hash.Write([]byte(n.Title)); err != nil {
		return
	}

	if _, err = hash.Write([]byte(n.Date.String())); err != nil {
		return
	}

	n.IdempotenceKey = hash.Sum(nil)

	return
}

func ConvertToNewsDomain(news *v1.News) *News {
	return &News{
		ID:             news.ID,
		IdempotenceKey: news.IdempotenceKey,
		Title:          news.Title,
		Date:           time.Unix(0, news.Date),
		RequestKey:     news.RequestKey,
	}
}

func ConvertToNewsProto(news *News) *v1.News {
	return &v1.News{
		ID:             news.ID,
		IdempotenceKey: news.IdempotenceKey,
		Title:          news.Title,
		Date:           news.Date.UnixNano(),
		RequestKey:     news.RequestKey,
	}
}
