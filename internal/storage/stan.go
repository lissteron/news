package storage

import (
	"time"

	"github.com/nats-io/stan.go"
	"github.com/spf13/viper"
	v1 "gitlab.com/lissteron/news/api/v1"
	"gitlab.com/lissteron/news/internal/domain"
)

func (s *Storage) Subscribe() (err error) {
	_, err = s.Stan.QueueSubscribe(
		viper.GetString("STAN_STORAGE_CREATE_SUBJ"),
		viper.GetString("STAN_CLIENT_GROUP"),
		s.CreateNews,
		stan.DurableName(viper.GetString("STAN_CLIENT_DURABLE_NAME")),
		stan.SetManualAckMode(),
	)
	if err != nil {
		s.Logger.Error("can't subscribe stan: %v", err)
		return
	}

	_, err = s.Stan.QueueSubscribe(
		viper.GetString("STAN_STORAGE_GET_SUBJ"),
		viper.GetString("STAN_CLIENT_GROUP"),
		s.GetNews,
		stan.DurableName(viper.GetString("STAN_CLIENT_DURABLE_NAME")),
		stan.SetManualAckMode(),
	)
	if err != nil {
		s.Logger.Error("can't subscribe stan: %v", err)
		return
	}

	return
}

func (s *Storage) CreateNews(msg *stan.Msg) {
	if s.SWaiter.NeedExit() {
		return
	}

	// Если сообщение протухло - скипаем
	if s.skipMessageByTTL(msg) {
		return
	}

	s.SWaiter.Add()

	go s.processCreateNew(msg)
}

func (s *Storage) processCreateNew(msg *stan.Msg) {
	defer s.SWaiter.Done()

	var (
		proto v1.News
		news  *domain.News
		err   error
	)

	if err = proto.Unmarshal(msg.Data); err != nil {
		s.Logger.Errorf("can't unmarshal proto: %v", err)
		return
	}

	if news, err = s.Save(domain.ConvertToNewsDomain(&proto)); err != nil {
		s.Logger.Errorf("save news error: %v", err)
		return
	}

	answer := domain.ConvertToNewsProto(news)

	b, err := answer.Marshal()
	if err != nil {
		s.Logger.Errorf("marshal news error: %v", err)
		return
	}

	if err = s.Stan.Publish(viper.GetString("STAN_CLIENT_SUBJ"), b); err != nil {
		s.Logger.Errorf("stan publish news error: %v", err)
		return
	}

	if err := msg.Ack(); err != nil {
		s.Logger.Errorf("stan ack error: %v", err)
	}
}

func (s *Storage) GetNews(msg *stan.Msg) {
	if s.SWaiter.NeedExit() {
		return
	}

	// Если сообщение протухло - скипаем
	if s.skipMessageByTTL(msg) {
		return
	}

	s.SWaiter.Add()

	go s.processGetNew(msg)
}

func (s *Storage) processGetNew(msg *stan.Msg) {
	defer s.SWaiter.Done()

	var proto v1.News
	if err := proto.Unmarshal(msg.Data); err != nil {
		s.Logger.Errorf("can't unmarshal proto: %v", err)
		return
	}

	news, err := s.Get(proto.ID)
	if err != nil {
		s.Logger.Errorf("get news error: %v", err)
		return
	}

	answer := domain.ConvertToNewsProto(news)
	answer.RequestKey = proto.RequestKey

	b, err := answer.Marshal()
	if err != nil {
		s.Logger.Errorf("marshal news error: %v", err)
		return
	}

	if err = s.Stan.Publish(viper.GetString("STAN_CLIENT_SUBJ"), b); err != nil {
		s.Logger.Errorf("stan publish news error: %v", err)
		return
	}

	if err := msg.Ack(); err != nil {
		s.Logger.Errorf("stan ack error: %v", err)
	}
}

func (s *Storage) skipMessageByTTL(msg *stan.Msg) bool {
	if time.Unix(0, msg.Timestamp).Add(s.StanTTL).Before(time.Now()) {
		if err := msg.Ack(); err != nil {
			s.Logger.Errorf("stan ack error: %v", err)
		}

		return true
	}

	return false
}
