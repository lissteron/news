package storage

import (
	"context"
	"database/sql"
	"errors"
	"time"

	"go.uber.org/zap"
	"gitlab.com/lissteron/news/internal/domain"
	"gitlab.com/lissteron/news/internal/pkg/stan"
	"gitlab.com/lissteron/news/pkg/sigwaiter"
)

type Storage struct {
	Logger  *zap.SugaredLogger
	DB      *CockroachDB
	Stan    *stan.Stan
	SWaiter *sigwaiter.Waiter
	StanTTL time.Duration
	Context context.Context
}

func NewStorage(
	ctx context.Context,
	logger *zap.SugaredLogger,
	swaiter *sigwaiter.Waiter,
	db *CockroachDB,
	stan *stan.Stan,
	stanTTL time.Duration,
) *Storage {
	return &Storage{
		Logger:  logger,
		DB:      db,
		Stan:    stan,
		SWaiter: swaiter,
		Context: ctx,
		StanTTL: stanTTL,
	}
}

func (s *Storage) Run() (err error) {
	if err = s.Subscribe(); err != nil {
		s.Logger.Errorf("subscribe error: %v", err)
		return
	}

	s.SWaiter.Add()

	go func() {
		select {
		case <-s.Context.Done():
		case <-s.SWaiter.WaitChan():
		}

		defer s.SWaiter.Done()
	}()

	return
}

func (s *Storage) Save(news *domain.News) (*domain.News, error) {
	err := s.DB.QueryRowContext(s.Context, `INSERT INTO news(idempotence_key, title, date)
		VALUES($1,$2,$3) ON CONFLICT (idempotence_key) DO UPDATE SET title=EXCLUDED.title 
		RETURNING id`,
		news.IdempotenceKey, news.Title, news.Date).Scan(&news.ID)
	if err != nil {
		s.Logger.Errorf("insert news error: %v", err)
		return nil, err
	}

	return news, nil
}

func (s *Storage) Get(id int64) (news *domain.News, err error) {
	news = new(domain.News)

	err = s.DB.GetContext(s.Context, news, `SELECT id, title, date FROM news WHERE id=$1`, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return news, nil
		}

		s.Logger.Errorf("select news error: %v", err)

		return
	}

	return
}
