package storage

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type CockroachDB struct {
	db     *sqlx.DB
	logger *zap.SugaredLogger
}

func NewCockroachDB(logger *zap.SugaredLogger) *CockroachDB {
	return &CockroachDB{logger: logger}
}

func (db *CockroachDB) Connect(ctx context.Context, connStr string) (err error) {
	db.db, err = sqlx.ConnectContext(ctx, "postgres", connStr)
	if err != nil {
		return
	}

	return
}

func (db *CockroachDB) QueryRowContext(
	ctx context.Context,
	query string,
	args ...interface{},
) *sql.Row {
	return db.db.QueryRowContext(ctx, query, args...)
}

func (db *CockroachDB) GetContext(
	ctx context.Context,
	dest interface{},
	query string,
	args ...interface{},
) (err error) {
	err = db.db.GetContext(ctx, dest, query, args...)
	if err != nil {
		return
	}

	return
}
