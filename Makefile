APP = news

easyjson:
	~/go/bin/easyjson internal/domain/

protoc:
	protoc --gofast_out=. ./api/v1/message.proto 
	
initdb:
	go run scripts/init/main.go 

gotest:
	go run scripts/test/main.go 
	
golint:
	golangci-lint run -v
