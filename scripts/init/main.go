package main

import (
	"fmt"
	"io/ioutil"
	"log"

	"github.com/jmoiron/sqlx"

	_ "github.com/lib/pq"
)

func main() {
	db, err := sqlx.Connect("postgres", fmt.Sprintf("postgresql://%s@%s/?sslmode=disable",
		"root", "localhost:26259",
	))
	if err != nil {
		log.Fatalln(err)
	}

	b, err := ioutil.ReadFile("database.sql")
	if err != nil {
		log.Fatalln(err)
	}

	_, err = db.Exec(string(b))
	if err != nil {
		log.Fatalln(err)
	}

	log.Println("database inited")
}
