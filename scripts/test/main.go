package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/lissteron/news/internal/domain"
)

const (
	getHost    = "http://localhost:28081"
	createHost = "http://localhost:28080"
)

func main() {
	log.SetFlags(log.Lshortfile)

	checkNotFound(1)
	checkNotFound(0)
	checkNotFound(-10)

	checkCreate()
	checkCreateDuplicate()

	checkNotFound(0)
	checkNotFound(-10)

	log.Println("success")
}

func checkNotFound(id int64) {
	_, code := getNews(id)

	if code != 404 {
		log.Fatalf("bad status code: %d", code)
	}
}

func checkCreate() {
	//nolint:gosec
	news := domain.News{Title: strconv.Itoa(rand.Int()), Date: time.Now().Truncate(time.Millisecond)}

	ans := postNews(&news)

	newNews, code := getNews(ans.ID)
	if code != 200 {
		log.Fatalf("bad status code: %d", code)
	}

	if newNews.Title != news.Title {
		log.Fatalln("bad title:", newNews.Title, news.Title)
	}

	if newNews.Date.UnixNano() != news.Date.UnixNano() {
		log.Fatalln("bad date:", newNews.Date, news.Date)
	}
}

func checkCreateDuplicate() {
	//nolint:gosec
	news := domain.News{Title: strconv.Itoa(rand.Int()), Date: time.Now().Truncate(time.Millisecond)}

	ans1 := postNews(&news)
	ans2 := postNews(&news)
	ans3 := postNews(&news)

	if ans1.ID != ans2.ID || ans2.ID != ans3.ID {
		log.Fatalln("bad ids: ", ans1.ID, ans2.ID, ans3.ID)
	}
}

func getNews(id int64) (*domain.News, int) {
	resp, err := http.Get(fmt.Sprintf("%s/news/get/%d", getHost, id))
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		log.Fatalln(err)
	}

	if resp.StatusCode != 200 {
		return nil, resp.StatusCode
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var answer domain.News
	if err = answer.UnmarshalJSON(body); err != nil {
		log.Fatalln(err)
	}

	return &answer, 200
}

func postNews(news *domain.News) *domain.News {
	b, err := news.MarshalJSON()
	if err != nil {
		log.Fatalln(err)
	}

	req, err := http.NewRequest("POST", fmt.Sprintf("%s/news/create", createHost), bytes.NewBuffer(b))
	if err != nil {
		log.Fatalln(err)
	}

	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}

	resp, err := client.Do(req)
	if resp != nil {
		defer resp.Body.Close()
	}

	if err != nil {
		log.Fatalln(err)
	}

	if resp.StatusCode != 201 {
		log.Fatalf("bad status code: %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	var answer domain.News
	if err = answer.UnmarshalJSON(body); err != nil {
		log.Fatalln(err)
	}

	return &answer
}
